# ringcloud-phapi
Библиотека для работы с [API ringcloud.ru](https://api.ringcloud.ru/docs).  
Для работы требуется PHP версии 5.3 и выше.

##### Начало
* [Установка]()
* [Авторизация]()

##### Аккаунт
* [Состояние баланса]()

##### Пользователи
* [Получение списка]()
* [Получение данных пользователя]()
* [Создание пользователя]()
* [Изменение данных]()
* [Работа с записями разговоров]()

##### Вызовы
* [Оригинация вызова]()
* [Получение списка каналов]()
* [Работа с текущими вызовами]()
* [Завершенные вызовы]()


---

## Установка

Библиотека доступна как пакет Composer. Для установки укажите его в секции
`require` вашего `composer.json`

```json
{  
    "require": {  
        "ringcloud/ringcloud-phapi": "*",
    }
}
```

В случае возникновения проблем и для получения дополнительной информации,
обратитесь к [документации Composer](https://getcomposer.org/doc/).



## Авторизация

 > Перед использованием API необходимо в личном кабинете сгенерировать ключ и пароль.

Для обращения к сервису создайте экземпляр класса [RingCloud\Phapi\Client](#markdown-header-ringcloudphapiclient),
передав ему ключ и пароль для авторизации:

```php
<?php

use RngCloud\Phapi\Client;

$key = "YOUR_API_KEY";
$password = "YOUR_API_PASSWORD";

$client = new Client($key, $password);
```

## Состояние баланса

Для получения данных о состоянии баланса используйте соответствующий метод объекта
[RingCloud\Phapi\Account\Account](#markdown-header-ringcloudphapiaccountaccount).

```php
<?php

echo $client->account()->getBalance(); // (float) 578.00
```


## Пользователи

Работа с пользователями осуществляется через экземпляр класса
[RingCloud\Phapi\Users\UserCollection](#markdown-header-ringcloudphapiusersusercollection), 
который можно получить вызвав метод [Client::users()](#markdown-header-ringcloudphapiclient).

Класс UserCollection реализует интерфейс Iterator, поэтому вы можете использовать его в циклах.

```php
<?php

foreach ($client->users() as $user) {
    get_class($user); // RingCloud\Phapi\Users\User
    // действия над пользователем
}
```


## Получение данных пользователя

Для получения информации о конкретном пользователе используйте метод коллекции
[RingCloud\Phapi\Users\UserCollection::get()]().

```php
<?php

$user = $client->users()->get('167965');
echo $user->getUsername() . "\n";
echo $user->getNumber();
```


## Создание пользователя

Используя метод [RingCloud\Phapi\Users\UserCollection::create()]() вы можете создать
нового пользователя. Данный метод принимает в качестве аргумента объект
[RingCloud\Phapi\Users\User]().

```php
<?php

$user = new User();
$user->setEmail('sam@example.com');
$user->setPassword('mySecret');
$user->setNumber(481);

$client->users()->create($user);
```


## Обновление пользователя

Для обновления данных пользователя установите необходимые параметры и вызовите метод
Используя метод [RingCloud\Phapi\Users\User::update()]().

```php
<?php

$user = $client->users()->get('1671138');

$user->setEmail('sam@example.com');
$user->setNumber(495);
$user->setPassword('newPassword');
$user->update();
```

## Записи разговоров

Используйте метод [RingCloud\Phapi\Users\User::getRecords()](), который возвращает коллекцию записей звонков для пользователя.
Данная коллекция реализует интерфейс Iterator, поэтому вы можете использовать ее в циклах.

```php
<?php

$user = $client->users()->get('167972');

foreach ($user->getRecords() as $record) {
    echo $record->getFileName(); // 02fadec3-4385-57ae-b78f-47af1327b5aa.mp3
    echo $record->getDate()->format('Y-m-d H:i:s'); // 2015-04-10 16:23:22
}
```

Для работы с одной записью используйте метод коллекции [RingCloud\Phapi\Records\RecordCollection::get()]().

```php
<?php

$user = $client->users()->get('167972');
$record = $user->getRecords()->get('02fadec3-4385-57ae-b78f-47af1327b5aa.mp3');
echo $record->getFileName();
```

Объект `Record` позволяет вам сохранить файл. Используйте для этого соответствующий метод.
 
```php
<?php

$user = $client->users()->get('167972');
$record = $user->getRecords()->get('02fadec3-4385-57ae-b78f-47af1327b5aa.mp3');
$record->download('/local/save/path/record.mp3');
``` 


## Вызовы

Работа с вызовами осуществляется через экземпляр класса 
[RingCloud\Phapi\Calls\CallCollection](), который можно получить вызвав метод
[Client::calls()]().

Класс CallCollection реализует интерфейс Iterator, поэтому вы можете использовать его в циклах.

```php
<?php

foreach ($client->calls() as $call) {
    // some action  
}
```

## Оригинация вызова

Объект CallCollection позволяет вам оригинировать вызов. Для этого необходимо 
указать набираемый номер телефона и имя пользователя, с которым будет установлено
соединение.


```php
<?php

use RingCloud\Phapi\Client;

$key = 'YOUR_API_KEY'
$password = 'YOUR_API_PASSWORD';

$client = new Client($key, $password);
$client->calls()->originate('79067776688', '167965');
```


---



## RingCloud\Phapi\Client

* [__construct()]()
* [calls()]() - возвращает коллекцию вызовов


#### Client::__construct($apiKey, $password)

##### Аргументы

* `$apiKey` - ключ, сгенерированный в личном кабинете
* `$password` - пароль, сгенерированный в личном кабинете


#### Client::calls()

##### Возвращаемое значение
* [RingCloud\Phapi\Calls\CallCollection]() - коллекция вызовов



---



## RingCloud\Phapi\Users\User

* [getEmail()]() - возвращает email пользователя
* [getNumber()]() - возвращает внутренний номер пользователя
* [getPassword()]() - возвращает пароль пользователя
* [getRecords()]() - возвращает коллекцию записей звонков пользователя
* [getVoiceMailBox()]() - возвращает состояние отправки голосовой почты. Смотрите `User::VOICE_MAIL_BOX_ON` и `User::VOICE_MAIL_BOX_OFF`
* [setEmail()]() - устанавливает значение email
* [setNumber()]() - устанавливает внутренний номер пользователя
* [setPassword()]() - устанавливает пароль пользователя
* [setVoiceMailBox()]() - устанавливает состояние отправки голосовой почты. Смотрите `User::VOICE_MAIL_BOX_ON` и `User::VOICE_MAIL_BOX_OFF` 
* [update()]() - обновление данных пользователя в сервисе


#### User::getEmail()

##### Возвращаемое значение
* `string` - email пользователя



#### User::getNumber()

##### Возвращаемое значение
* `int` - внутренний номер пользователя

#### User::getPassword()

##### Возвращаемое значение
* `string` - пароль пользователя

#### User::getRecords()

##### Возвращаемое значение
* [RingCloud\Phapi\Records\RecordCollection]() - коллекция записей пользователя


#### User::getVoiceMailBox()

##### Возвращаемое значение
* `string` - состояние отправки голосовой почты. Смотрите `User::VOICE_MAIL_BOX_ON` и `User::VOICE_MAIL_BOX_OFF` 


#### User::setEmail($email)

##### Аргументы
* `string` - email пользователя


#### User::setNumber($number)

##### Аргументы
* `$number` - внутренний номер пользователя

#### User::setPassword($password)

##### Аргументы
* `$password` - новый пароль пользователя


#### User::setVoiceMailBox($state)

##### Аргументы
* `$state` - состояние отправки голосовой почты. Смотрите `User::VOICE_MAIL_BOX_ON` и `User::VOICE_MAIL_BOX_OFF` 

#### User::update()



---


## RingCloud\Phapi\Users\UserCollection

* [get($username)]()
* [create($user)]()

#### UserCollection::get($username)

##### Аргументы

* `$username` - пользователь (SIP-логин)

#### UserCollection::create($user)

##### Аргументы

* `$user` - объект типа [RingCloud\Phapi\Users\User]()

##### Возвращаемое значение
* [RingCloud\Phapi\Users\User]() - объект содержащий данные созданного пользователя



---


## RingCloud\Phapi\Calls\CallCollection

* [originate]($targetNumber, $username)

#### CallCollection::originate($targetNumber, $username)

##### Аргументы

* `$targetNumber` - набираемый номер телефона
* `$username` - внутренний пользователь, с которым произойдет соединение



---


## RingCloud\Phapi\Records\Record

* [download()]() - сохраняет файл записи
* [getFileName()]() - возвращает имя файла записи
* [getDate()]() - возвращает объект \DateTime, содержащий время создания записи
* [getUser()]() - возвращает объект пользователя, с которым связана данная запись

#### Record::download($savePath)

##### Аргументы
* `$savePath` - путь к сохраняемому файлу

##### Возвращаемое значение
* `bool`

#### Record::getFileName()

##### Возвращаемое значение
* `string` - имя файла записи

#### Record::getDate()

##### Возвращаемое значение
* `\DateTime` - дата создания файла записи

#### Record::getUser()

##### Возвращаемое значение
* RingCloud\Phapi\Users\User - пользователя, с которым связана данная запись



---


## RingCloud\Phapi\Records\RecordCollection

* [get($fileName)]()

#### RecordCollection::get($fileName)

##### Аргументы

* `$fileName` - имя файла записи