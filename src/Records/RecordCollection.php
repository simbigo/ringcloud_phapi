<?php

namespace RingCloud\Phapi\Records;


use RingCloud\Phapi\Base\BaseCollection;
use RingCloud\Phapi\Base\Exception;
use RingCloud\Phapi\Base\Request;
use RingCloud\Phapi\Client;
use RingCloud\Phapi\Users\User;

/**
 * Class RecordCollection
 * @package RingCloud\Phapi\Records
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class RecordCollection extends BaseCollection
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param Client $client
     * @param User $user
     */
    public function __construct(Client $client, User $user)
    {
        parent::__construct($client);
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $fileName
     * @return Record
     * @throws Exception
     */
    public function get($fileName)
    {
        if (!$this->isLoaded) {
            $this->loadCollection();
        }
        if (!isset($this->indexedCollection[$fileName])) {
            throw new Exception('File does not exist: ' . $fileName);
        }
        return $this->indexedCollection[$fileName];
    }

    /**
     * @param bool $reload
     * @return mixed
     */
    protected function loadCollection($reload = false)
    {
        if (!$reload && $this->isLoaded) {
            return true;
        }
        $path = 'users/' . $this->getUser()->getUsername() . '/records';
        $response = $this->getApiClient()->sendRequest($path, Request::METHOD_GET);
        $user = $this->getUser();
        foreach ($response as $recordData) {
            $this->collection[] = new Record($user, $recordData[0], $recordData[1]);
        }
        $this->indexCollection('fileName');
        $this->isLoaded = true;
        return $this;
    }
}