<?php

namespace RingCloud\Phapi\Records;


use RingCloud\Phapi\Base\BaseObject;
use RingCloud\Phapi\Base\Exception;
use RingCloud\Phapi\Base\Request;
use RingCloud\Phapi\Users\User;

/**
 * Class Record
 * @package RingCloud\Phapi\Records
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class Record extends BaseObject
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var
     */
    private $fileName;
    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @param User $user
     * @param string $fileName
     * @param string $recordDate
     */
    public function __construct(User $user, $fileName, $recordDate)
    {
        $this->user = $user;
        $this->fileName = $fileName;
        $this->date = \DateTime::createFromFormat('Y-m-d H:i:s', $recordDate);
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $savePath
     * @return int
     * @throws Exception
     */
    public function download($savePath)
    {
        $path = 'users/' . $this->getUser()->getUsername() . '/records/' . $this->getFileName();
        $response = $this->getUser()->getApiClient()->sendRequest($path, Request::METHOD_GET, null, true);
        if (!is_writable(dirname($savePath))) {
            throw new Exception('Directory "' . dirname($savePath) . '" is not writable');
        }
        return file_put_contents($savePath, $response);
    }
}