<?php

namespace RingCloud\Phapi\Account;


use RingCloud\Phapi\Base\BaseObject;
use RingCloud\Phapi\Base\Request;
use RingCloud\Phapi\Client;

/**
 * Class User
 * @package RingCloud\Phapi\Account
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class Account extends BaseObject
{
    /**
     * @var int
     */
    private $accountId;

    public function __construct(Client $client = null, $accountId)
    {
        parent::__construct($client);
        $this->accountId = $accountId;
    }

    public function getBalance()
    {
        return $this->getApiClient()->sendRequest('accounts/' . $this->getAccountId() . '/balance', Request::METHOD_GET);
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }
}
