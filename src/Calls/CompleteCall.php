<?php

namespace RingCloud\Phapi\Calls;

use RingCloud\Phapi\Records\Record;


/**
 * Class CompleteCall
 * @package RingCloud\Phapi\Calls
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class CompleteCall extends Call
{
    public function getDirection()
    {
        return $this->get('direction');
    }

    public function getStartTime()
    {
        $dt = $this->get('call_start_time');
        if (is_string($dt)) {
            $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $dt);
            $this->callData['call_start_time'] = $dt;
        }
        return $dt;
    }

    public function getRecord()
    {
        $recordFile = $this->get('rec_file');
        if ($recordFile == "None") {
            return null;
        }
        $recordFile = explode('/', $recordFile);
        $file = array_pop($recordFile);
        array_pop($recordFile);
        $userId = array_pop($recordFile);
        $user = $this->getApiClient()->users()->get($userId);
        return new Record($user, $file, $this->get('call_end_time'));
    }
}
