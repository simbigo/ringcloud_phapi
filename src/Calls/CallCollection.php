<?php

namespace RingCloud\Phapi\Calls;


use RingCloud\Phapi\Base\BaseCollection;
use RingCloud\Phapi\Base\Request;

/**
 * Class CallCollection
 * @package RingCloud\Phapi\Calls
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class CallCollection extends BaseCollection
{
    /**
     * @var Channel[]
     */
    private $channels;
    private $activeCalls;
    private $completeCalls;
    /**
     * @param $targetNumber
     * @param $username
     * @return bool
     * @throws \RingCloud\Phapi\Base\Exception
     */
    public function originate($targetNumber, $username)
    {
        $params = array(
            'num' => $targetNumber,
            'user' => $username
        );
        return (bool) $this->getApiClient()->sendRequest('calls', Request::METHOD_POST, $params);
    }

    /**
     * @return Channel[]
     * @throws \RingCloud\Phapi\Base\Exception
     */
    public function channels()
    {
        if ($this->channels === null) {
            $this->channels = array();

            $path = 'calls/channels';
            $client = $this->getApiClient();
            $response = $client->sendRequest($path, Request::METHOD_GET);
            foreach ($response as $channel) {
                $this->channels[] = new Channel($client, $channel);
            }
        }
        return $this->channels;
    }

    /**
     * @return CompleteCall[]
     * @throws \RingCloud\Phapi\Base\Exception
     */
    public function complete()
    {
        if ($this->completeCalls === null) {
            $this->completeCalls = array();

            $path = 'calls/complete';
            $client = $this->getApiClient();
            $response = $client->sendRequest($path, Request::METHOD_GET);
            foreach ($response as $call) {
                $this->completeCalls[] = new CompleteCall($client, $call);
            }
        }
        return $this->completeCalls;
    }

    public function active()
    {
        if ($this->activeCalls === null) {
            $this->activeCalls = array();

            $path = 'calls/active';
            $client = $this->getApiClient();
            $response = $client->sendRequest($path, Request::METHOD_GET);
            foreach ($response as $call) {
                $this->activeCalls[] = new ActiveCall($client, $call);
            }
        }
        return $this->activeCalls;
    }

    /**
     * @param bool $reload
     * @return mixed
     */
    protected function loadCollection($reload = false)
    {
        $this->collection = array_merge($this->active(), $this->complete());
        $this->isLoaded = true;
        return $this;
    }
}