<?php

namespace RingCloud\Phapi\Calls;


use RingCloud\Phapi\Base\BaseObject;
use RingCloud\Phapi\Client;

/**
 * Class Call
 * @package RingCloud\Phapi\Calls
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
abstract class Call extends BaseObject
{
    const STATUS_ACTIVE = 'current';
    const STATUS_COMPLETE = 'complete';

    const DIRECTION_INCOMING = 'incoming';
    const DIRECTION_OUTGOING = 'outgoing';
    const DIRECTION_INTERNAL = 'internal';

    protected $callData;

    protected function get($key)
    {
        return isset($this->callData[$key]) ? $this->callData[$key] : null;
    }

    public function __construct(Client $client = null, $callData)
    {
        parent::__construct($client);
        $this->callData = $callData;
    }

    public function isActive()
    {
        return $this->get('status') == self::STATUS_ACTIVE;
    }

    public function isComplete()
    {
        return $this->get('status') == self::STATUS_COMPLETE;
    }

    public function getStatus()
    {
        return $this->get('status');
    }

    public function getSrc()
    {
        return $this->get('src');
    }

    public function getDst()
    {
        return $this->get('dst');
    }

    public function asArray()
    {
        return $this->callData;
    }
}
