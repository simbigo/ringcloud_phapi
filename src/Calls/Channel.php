<?php

namespace RingCloud\Phapi\Calls;


use RingCloud\Phapi\Base\BaseObject;
use RingCloud\Phapi\Base\Request;
use RingCloud\Phapi\Client;

/**
 * Class Channel
 * @package RingCloud\Phapi\Calls
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class Channel extends BaseObject
{
    /**
     * @var bool
     */
    private $isLoaded = false;
    /**
     * @var array
     */
    private $channelData = array();

    /**
     * @param Client $client
     * @param $channelName
     */
    public function __construct(Client $client = null, $channelName)
    {
        parent::__construct($client);
        $this->channelData['Channel'] = $channelName;
    }

    /**
     * @param $key
     * @return null
     */
    protected function get($key)
    {
        if (!$this->isLoaded && $key != 'Channel') {
            $this->load();
        }
        return isset($this->channelData[$key]) ? $this->channelData[$key] : null;
    }

    /**
     * @return null
     */
    public function getConnectedLineNum()
    {
        return $this->get('ConnectedLineNum');
    }

    /**
     * @return null
     */
    public function getCallerIDNum()
    {
        return $this->get('CallerIDNum');
    }

    /**
     * @return null
     */
    public function getSeconds()
    {
        return $this->get('Seconds');
    }

    /**
     * @return null
     */
    public function getConnectedLineName()
    {
        return $this->get('ConnectedLineName');
    }

    /**
     * @return null
     */
    public function getCallerIDName()
    {
        return $this->get('CallerIDName');
    }

    /**
     * @return null
     */
    public function getChannelName()
    {
        return $this->get('Channel');
    }

    /**
     * @return $this|bool
     * @throws \RingCloud\Phapi\Base\Exception
     */
    protected function load()
    {
        if ($this->isLoaded) {
            return true;
        }

        $path = 'calls/channels/' . $this->getChannelName();
        $response = $this->getApiClient()->sendRequest($path, Request::METHOD_GET);
        $this->channelData = $response;
        $this->isLoaded = true;
        return $this;
    }
} 