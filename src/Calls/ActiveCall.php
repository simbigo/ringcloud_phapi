<?php

namespace RingCloud\Phapi\Calls;


/**
 * Class ActiveCall
 * @package RingCloud\Phapi\Calls
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class ActiveCall extends Call
{
    public function getSeconds()
    {
        return $this->get('seconds');
    }
}
