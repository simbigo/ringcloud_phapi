<?php

namespace RingCloud\Phapi;

use RingCloud\Phapi\Account\Account;
use RingCloud\Phapi\Base\Exception;
use RingCloud\Phapi\Calls\ActiveCall;
use RingCloud\Phapi\Calls\Call;
use RingCloud\Phapi\Calls\CallCollection;
use RingCloud\Phapi\Base\Request;
use RingCloud\Phapi\Calls\CompleteCall;
use RingCloud\Phapi\Users\User;
use RingCloud\Phapi\Users\UserCollection;


/**
 * Class Client
 * @package RingCloud\Phapi
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class Client
{
    /**
     *
     */
    const API_VERSION = 'v1';
    /**
     *
     */
    const STATUS_SUCCESS = 'success';
    /**
     *
     */
    const STATUS_ERROR = 'error';
    /**
     *
     */
    public $apiUri = 'https://api.ringcloud.ru';
    /**
     * @var
     */
    private $apiKey;
    /**
     * @var string
     */
    private $apiHash;
    /**
     * @var \RingCloud\Phapi\Base\Request
     */
    private $request;
    /**
     * @var CallCollection
     */
    private $callCollection;
    /**
     * @var UserCollection
     */
    private $userCollection;
    /**
     * @var Account
     */
    private $account;

    /**
     * @param $apiKey
     * @param $password
     */
    public function __construct($apiKey, $password)
    {
        $this->apiKey = $apiKey;
        $this->apiHash = md5($this->apiKey . $password);
        $this->request = new Request($this);
        // $this->logFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'ringcloud.api.log';
    }

    /**
     * @return UserCollection|User[]
     */
    public function users()
    {
        if ($this->userCollection === null) {
            $this->userCollection = new UserCollection($this);
        }
        return $this->userCollection;
    }

    /**
     * @return CallCollection|ActiveCall[]|CompleteCall
     */
    public function calls()
    {
        if ($this->callCollection === null) {
            $this->callCollection = new CallCollection($this);
        }
        return $this->callCollection;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Account
     */
    public function account()
    {
        if ($this->account === null) {
            $parts = explode('_', $this->apiKey);
            $accountId = $parts[0];
            $this->account = new Account($this, $accountId);
        }
        return $this->account;
    }

    /**
     * @param $path
     * @param $method
     * @param array $postData
     * @param bool $returnRaw
     * @return mixed|string
     * @throws Exception
     */
    public function sendRequest($path, $method, array $postData = null, $returnRaw = false)
    {
        if ($postData !== null) {
            $postData = ['params' => $postData];
        }
        $url = $this->apiUri . '/' . self::API_VERSION . '/' . $path . '?api_key=' . $this->apiKey . '&hash=' . $this->apiHash;
        //$logMessage = $method . ' ' . $url . "\n";
        if ($postData !== null) {
            //$logMessage .= json_encode($postData) . "\n";
        }
        $request = $this->getRequest();
        $request->setUrl($url);
        $request->setMethod($method);
        $request->setPostData($postData);

        $response = $request->send();
        //$logMessage .= $response;
        //$this->log($logMessage);
        if ($returnRaw) {
            return $response;
        }
        $response = $this->decodeJson($response);
        if ($response['status'] == self::STATUS_ERROR) {
            throw new Exception($response['message']);
        }
        return $response['data'];
    }

    /**
     * @return string
     */
    private function getJsonError()
    {
        if (function_exists('json_last_error_msg')) {
            return json_last_error_msg();
        }

        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                $error = '';
        }
        return $error;
    }

    /**
     * @param string $json
     * @param bool $asArray
     * @return mixed
     * @throws Exception
     */
    private function decodeJson($json, $asArray = true)
    {
        $result = json_decode($json, $asArray);
        if ($result === null) {
            throw new Exception('JSON decode failed: ' . $this->getJsonError());
        }
        return $result;
    }
}