<?php

namespace RingCloud\Phapi\Base;

/**
 * Class Request
 * @package RingCloud\Phapi\Base
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class Request extends BaseObject
{
    /**
     *
     */
    const METHOD_GET = 'GET';
    /**
     *
     */
    const METHOD_POST = 'POST';

    /**
     * @var
     */
    private $url;
    /**
     * @var string
     * @see Request::METHOD_GET and Request::METHOD_POST
     */
    private $method;
    /**
     * @var array
     */
    private $postData = array();

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getPostData()
    {
        return $this->postData;
    }

    /**
     * @param array $postData
     */
    public function setPostData($postData)
    {
        $this->postData = $postData;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function send()
    {
        if (function_exists('curl_init')) {
            return $this->curlRequest();
        }
        return $this->httpContextRequest();
    }

    /**
     * @return string
     */
    private function httpContextRequest()
    {
        $options = array('http' => array('method' => $this->getMethod()));

        if ($this->method == self::METHOD_POST) {
            $options['http']['header'] = 'Content-Type: application/json';
            $options['http']['content'] = $this->postData;
        }

        $context = stream_context_create($options);

        return file_get_contents($this->getUrl(), null, $context);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function curlRequest()
    {
        $curl = curl_init($this->getUrl());
        curl_setopt($curl, $this->method == self::METHOD_POST ? CURLOPT_POST : CURLOPT_HTTPGET, true);
        if ($this->method == self::METHOD_POST) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->postData));
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $response = curl_exec($curl);
        if ($response === false) {
            $error = curl_error($curl);
            throw new Exception($error);
        }
        return $response;
    }

}