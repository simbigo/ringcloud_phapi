<?php

namespace RingCloud\Phapi\Base;


use RingCloud\Phapi\Client;

/**
 * Class BaseObject
 * @package RingCloud\Phapi\Base
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
abstract class BaseObject
{
    /**
     * @var Client
     */
    protected $apiClient;

    /**
     * @return Client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * @param Client $client
     */
    public function setApiClient(Client $client)
    {
        $this->apiClient = $client;
    }

    /**
     * @param Client $client
     */
    public function __construct(Client $client = null)
    {
        $this->apiClient = $client;
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }
        throw new Exception('Undefined property ' . get_class($this) . '::$' . $name);
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     * @throws Exception
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            return $this->$setter($value);
        }
        throw new Exception('Undefined property ' . get_class($this) . '::$' . $name);
    }
} 