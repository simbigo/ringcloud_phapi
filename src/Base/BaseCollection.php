<?php

namespace RingCloud\Phapi\Base;


/**
 * Class BaseCollection
 * @package RingCloud\Phapi\Base
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
abstract class BaseCollection extends BaseObject implements \Iterator
{
    /**
     * @var array
     */
    protected $collection = array();
    /**
     * @var int
     */
    protected $collectionIndex;
    /**
     * @var array
     */
    protected $indexedCollection = array();
    /**
     * @var string
     */
    protected $defaultIndex = 'id';
    /**
     * @var bool
     */
    protected $isLoaded = false;

    /**
     * @param $byAttribute
     */
    protected function indexCollection($byAttribute)
    {
        $this->indexedCollection = array();
        foreach ($this->collection as $item) {
            if (is_object($item)) {
                $index = $item->$byAttribute;
            } else {
                $index = $item[$byAttribute];
            }
            $this->indexedCollection[$index] = $item;
        }
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->collection[$this->collectionIndex];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->collectionIndex++;
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->collectionIndex;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->collection[$this->collectionIndex]);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        if (!$this->isLoaded) {
            $this->loadCollection();
        }
        $this->collectionIndex = 0;
    }

    /**
     * @return int
     */
    public function count()
    {
        if (!$this->isLoaded) {
            $this->loadCollection();
        }
        return count($this->collection);
    }

    /**
     * @param bool $reload
     * @return mixed
     */
    abstract protected function loadCollection($reload = false);
}