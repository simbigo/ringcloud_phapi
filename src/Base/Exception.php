<?php

namespace RingCloud\Phapi\Base;


/**
 * Class Exception
 * @package RingCloud\Phapi\Base
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class Exception extends \Exception
{

}
