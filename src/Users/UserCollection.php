<?php

namespace RingCloud\Phapi\Users;


use RingCloud\Phapi\Base\BaseCollection;
use RingCloud\Phapi\Base\Exception;
use RingCloud\Phapi\Base\Request;

/**
 * Class UserCollection
 * @package RingCloud\Phapi\Users
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 */
class UserCollection extends BaseCollection
{
    /**
     * @param bool $reload
     * @return mixed
     */
    protected function loadCollection($reload = false)
    {
        if (!$reload && $this->isLoaded) {
            return true;
        }
        $response = $this->getApiClient()->sendRequest('users', Request::METHOD_GET);
        foreach ($response as $username) {
            if (!isset($this->indexedCollection[$username])) {
                $this->collection[] = new User($this->getApiClient(), $username);
            }
        }
        $this->indexCollection('username');
        $this->isLoaded = true;
        return $this;
    }

    /**
     * @param $username
     * @return User
     * @throws Exception
     */
    public function get($username)
    {
        if (isset($this->indexedCollection[$username])) {
            return $this->indexedCollection[$username];
        }

        if ($this->isLoaded) {
            throw new Exception('User does not exist: ' . $username);
        }

        $user = new User($this->getApiClient(), $username);
        //$user->getAccountId(); // load any data
        $this->collection[] = $user;
        $this->indexCollection('username');

        return $this->indexedCollection[$username];
    }

    /**
     * @param User $user
     * @return bool
     * @throws Exception
     */
    public function create(User $user)
    {
        $user->setIsNew(true);
        $params = array(
            'mail' => $user->getEmail(),
            'password' => $user->getPassword(),
            'num' => $user->getNumber()
        );

        $username = $this->getApiClient()->sendRequest('users/create', Request::METHOD_POST, $params);
        $user->setUsername($username);
        $user->setIsNew(false);
        $this->collection[] = $user;
        $this->indexCollection('username');
    }
}