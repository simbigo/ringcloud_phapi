<?php

namespace RingCloud\Phapi\Users;


use RingCloud\Phapi\Base\BaseObject;
use RingCloud\Phapi\Base\Exception;
use RingCloud\Phapi\Base\Request;
use RingCloud\Phapi\Client;
use RingCloud\Phapi\Records\Record;
use RingCloud\Phapi\Records\RecordCollection;

/**
 * Class User
 * @package RingCloud\Phapi\Users
 * @author Andrey Bondarev <a.b@ringcloud.ru>
 *
 * @property string $username
 * @property string $email
 * @property string $accountId
 * @property string $number
 * @property string $password
 * @property string $voiceMailBox
 */
class User extends BaseObject
{
    /**
     *
     */
    const VOICE_MAIL_BOX_ON = 'on';
    /**
     *
     */
    const VOICE_MAIL_BOX_OFF = 'off';

    /**
     * @var array
     */
    private $userData;
    /**
     * @var array
     */
    private $updatedUserData;
    /**
     * @var bool
     */
    private $isLoaded = false;
    /**
     * @var
     */
    private $records;
    /**
     * @var bool
     */
    private $isNew = false;

    /**
     * @param Client $client
     * @param string $username
     * @return mixed
     */
    public function __construct(Client $client = null, $username = null)
    {
        parent::__construct($client);
        $this->userData['user'] = $username;
    }

    /**
     * @param $bool
     */
    public function setIsNew($bool)
    {
        $this->isNew = (bool) $bool;
    }

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    protected function get($key, $default = null)
    {
        if (!$this->isLoaded && $key != 'user' && !$this->isNew) {
            $this->load();
        }
        if (isset($this->updatedUserData[$key])) {
            return $this->updatedUserData[$key];
        }
        return isset($this->userData[$key]) ? $this->userData[$key] : $default;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    protected function set($key, $value)
    {
        $this->updatedUserData[$key] = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->get('mail');
    }

    /**
     * @param mixed $email
     * @return \RingCloud\Phapi\Users\User
     */
    public function setEmail($email)
    {
        return $this->set('mail', $email);
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->get('num');
    }

    /**
     * @param mixed $number
     * @return \RingCloud\Phapi\Users\User
     */
    public function setNumber($number)
    {
        return $this->set('num', $number);
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->get('password');
    }

    /**
     * @param mixed $password
     * @return \RingCloud\Phapi\Users\User
     */
    public function setPassword($password)
    {
        return $this->set('password', $password);
    }

    /**
     * @return mixed
     */
    public function getVoiceMailBox()
    {
        return $this->get('voice_mail_box');
    }

    /**
     * @param $value
     * @throws Exception
     * @return \RingCloud\Phapi\Users\User
     */
    public function setVoiceMailBox($value)
    {
        if ($value !== self::VOICE_MAIL_BOX_OFF && $value !== self::VOICE_MAIL_BOX_ON) {
            throw new Exception('Invalid argument value: ' . __CLASS__ . '::' . __METHOD__ . '()');
        }
        return $this->set('voice_mail_box', $value);
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->get('user');
    }

    /**
     * @param $username
     * @return User
     */
    public function setUsername($username)
    {
        return $this->set('user', $username);
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->get('user');
    }

    /**
     * @return bool
     */
    public function update()
    {
        if (!is_array($this->updatedUserData)) {
            return true;
        }
        unset($this->updatedUserData['user']);
        if (count($this->updatedUserData) > 0) {
            foreach ($this->updatedUserData as $key => $value) {
                switch ($key) {
                    case 'password':
                        $this->updatePassword($value);
                        break;
                    case 'mail':
                        $this->updateEmail($value);
                        break;
                    case 'num':
                        $this->updateNumber($value);
                        break;
                    case 'voice_mail_box':
                        $this->updateVoiceMailBox($value);
                        break;
                }
            }
            $this->isLoaded = false;
            $this->load();
        }
        return true;
    }

    /**
     * @param $password
     * @return bool
     * @throws Exception
     */
    protected function updatePassword($password)
    {
        return (bool) $this->getApiClient()->sendRequest(
            'users/' . $this->getUsername() . '/update_password',
            Request::METHOD_POST,
            array('password' => $password)
        );
    }

    /**
     * @param $state
     * @return bool
     * @throws Exception
     */
    protected function updateVoiceMailBox($state)
    {
        $action = $state == self::VOICE_MAIL_BOX_ON ? '_on' : '_off';
        return (bool) $this->getApiClient()->sendRequest(
            'users/' . $this->getUsername() . '/voice_mail_box' . $action,
            Request::METHOD_POST
        );
    }

    /**
     * @param $number
     * @return bool
     * @throws Exception
     */
    protected function updateNumber($number)
    {
        return (bool) $this->getApiClient()->sendRequest(
            'users/' . $this->getUsername() . '/update_extension_number',
            Request::METHOD_POST,
            array('num' => $number)
        );
    }

    /**
     * @param $email
     * @return bool
     * @throws Exception
     */
    protected function updateEmail($email)
    {
        return (bool) $this->getApiClient()->sendRequest(
            'users/' . $this->getUsername() . '/update_email',
            Request::METHOD_POST,
            array('mail' => $email)
        );
    }

    /**
     * @param bool $cache
     * @return RecordCollection|Record[]
     */
    public function getRecords($cache = true)
    {
        if (!$cache || $this->records === null) {
            $this->records = new RecordCollection($this->getApiClient(), $this);
        }
        return $this->records;
    }

    /**
     * @return $this|bool
     * @throws Exception
     */
    protected function load()
    {
        if ($this->isLoaded) {
            return true;
        }
        $response = $this->getApiClient()->sendRequest('users/' . $this->getUsername(), Request::METHOD_GET);
        $this->userData = $response;
        $this->updatedUserData = array();
        $this->isLoaded = true;
        return $this;
    }
}
